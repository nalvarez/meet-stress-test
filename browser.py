# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
import logging
import os

logging.basicConfig(format='%(asctime)-15s [%(name)s] %(message)s')
l = logging.getLogger('webdriver')
l.setLevel(logging.DEBUG)

class BrowserInstance:
    def __init__(self):
        self.browser = None

    def __del__(self):
        if self.browser:
            self.quit_browser()

    def start_browser(self, browser_name):
        assert browser_name in ('firefox', 'chrome')

        l.info("Starting %s", browser_name)
        start_time = time.time()

        if browser_name == 'firefox':
            firefoxOptions = webdriver.FirefoxOptions()
            firefoxOptions.headless = True
            self.browser = webdriver.Firefox(options=firefoxOptions)
        elif browser_name == 'chrome':
            chromeOptions = webdriver.ChromeOptions()
            chromeOptions.headless = True
            chromeOptions.add_argument("disable-gpu")
            chromeOptions.add_argument("disable-dev-shm-usage")
            # can't use sandbox on Docker unless --privileged
            if os.environ.get('DISABLE_SANDBOX') == '1':
                chromeOptions.add_argument("no-sandbox")
            self.browser = webdriver.Chrome(options=chromeOptions)

        l.info("We started %s in %.2f seconds" % (browser_name, time.time()-start_time))

    def quit_browser(self):
        self.browser.quit()
        self.browser = None

    def join_call(self, room_url, user_name):
        start_time = time.time()
        l.info("Load page")
        self.browser.get(room_url)

        l.info("Typing username")
        name_field = self.browser.find_element_by_xpath("//input[@placeholder='Enter your name!']")
        name_field.clear()
        name_field.send_keys(user_name)
        l.info("Joining call")
        self.browser.find_element_by_xpath("//button[@type='submit'][contains(@class, 'join-form')]").click()
        l.info("Wait for audio options")
        listen_btn = WebDriverWait(self.browser, 30).until(lambda x: x.find_element_by_xpath("//span/button[@aria-label='Listen only']"))
        l.info("Click listen only")
        listen_btn.click()
        l.info("We got into the call in %.2f seconds" % (time.time()-start_time))
