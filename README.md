This repository contains scripts
to do distributed stress-testing of meet.kde.org
in advance of Akademy.
Volunteers can run a node,
and we can make nodes start a browser (or many!)
that all join a BigBlueButton room.
Then we can see how many simultaneous users
the server can cope with.

You can help by running a node yourself!

Using Docker:
```
docker run --rm --init -i nalvarez17/kde-meet-test yournamehere
```
`--init` prevents zombie processes.
The -i is needed if you want to be able to stop it with Ctrl-C.
The name is optional, but it lets us know who is running each node
in case we need to contact you (eg. "hey we finished the test, you can stop running it now").


Or manually:

- You need Python 3 (I tested on 3.7, older might work)
- You need either Chrome/Chromium (with chrome-driver/chromium-driver) or
  Firefox (with geckodriver).
- Install the dependencies listed in requirements.txt, possibly using a virtualenv.
- Run `python3 node.py yourname`. The name is optional, but it lets us know who
  is running each node.
- Stay in contact over IRC :) Maybe I'll need you to update and restart the node, etc.

```
virtualenv -ppython3 venv
. venv/bin/activate
pip3 install -r requirements.txt
python3 node.py yourname
```

Yes, this is over-engineered and overkill.
