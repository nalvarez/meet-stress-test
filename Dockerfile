FROM debian:buster AS ffxbuilder

WORKDIR /tmp/builder

RUN apt-get update && \
    apt-get install -y eatmydata

RUN eatmydata apt-get install -y --no-install-recommends \
    xz-utils \
    zip unzip \
    curl ca-certificates \
    make \
    patch \
    clang llvm \
    yasm \
    python \
    python3 \
    python3-distutils \
    pkg-config \
    autoconf2.13 \
    libpulse-dev \
    libdbus-glib-1-dev \
    libgtk-3-dev \
    libgtk2.0-dev \
    libxt-dev \
    nodejs \
    && apt-get clean

RUN useradd -d $PWD build && chown build $PWD
USER build

RUN curl -Sf https://archive.mozilla.org/pub/firefox/releases/77.0.1/source/firefox-77.0.1.source.tar.xz | tar -xJ && \
    rm -rf firefox-77.0.1/obj-x86_64-pc-linux-gnu

RUN cd && curl -Sf -o rustup.sh https://sh.rustup.rs && \
    sh rustup.sh -y --default-toolchain 1.44.1 && \
    PATH=$HOME/.cargo/bin:$PATH cargo install cbindgen

COPY mozconfig firefox-77.0.1/.mozconfig
RUN cd firefox-77.0.1 && SHELL=/bin/bash PATH=$HOME/.cargo/bin:$PATH ./mach configure
RUN cd firefox-77.0.1 && SHELL=/bin/bash PATH=$HOME/.cargo/bin:$PATH ./mach build -j4

COPY noshm.diff noshm.diff
RUN cd firefox-77.0.1 && patch -p1 < $HOME/noshm.diff
RUN cd firefox-77.0.1 && SHELL=/bin/bash PATH=$HOME/.cargo/bin:$PATH ./mach build -j4
RUN cd firefox-77.0.1 && SHELL=/bin/bash PATH=$HOME/.cargo/bin:$PATH ./mach install

FROM python:3.7-slim

WORKDIR /usr/src/app

RUN apt-get update && \
    apt-get install -y eatmydata && \
    eatmydata apt-get install -y --no-install-recommends libgtk-3-0 libdbus-glib-1-2 libx11-xcb1 libxt6 && \
    apt-get clean

RUN useradd -d $PWD node && chown node $PWD
USER node

COPY --from=ffxbuilder /tmp/builder/local/firefox/lib/firefox/ ./firefox/
COPY --from=ffxbuilder /tmp/builder/firefox-77.0.1/obj-x86_64-pc-linux-gnu/testing/geckodriver/x86_64-unknown-linux-gnu/release/geckodriver ./firefox/geckodriver

COPY requirements.txt ./
RUN pip install --no-cache-dir --target . -r requirements.txt
COPY browser.py node.py ./

ENV PATH=$PATH:./firefox LD_LIBRARY_PATH=./firefox

ENTRYPOINT ["eatmydata", "python3", "./node.py"]
