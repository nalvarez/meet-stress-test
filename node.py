#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import asyncio
import threading
import logging
import aiohttp
import random
from yarl import URL
from enum import Enum

logging.basicConfig(format='%(asctime)-15s [%(name)s] %(message)s')

from browser import BrowserInstance

l = logging.getLogger('node')
l.setLevel(logging.DEBUG)

BASEURL=URL("http://meet-test.nicolas17.xyz:12345/")

class State(Enum):
    INITIAL=0
    STARTING=1
    RUNNING=2
    STOPPING=3
    STOPPED=4

class BrowserThread:
    """
    Runs a BrowserInstance in a thread and wraps it in an async API
    """
    def __init__(self):
        self.state = State.INITIAL
        self.browser_instance = None
        self.thread = None
        self.stop_event = threading.Event()

        self.room_url = ""
        self.user_name = "test"
        self.browser_name = "firefox"

    # awaitable
    def start(self):
        assert self.state == State.INITIAL
        self.start_future = asyncio.Future()
        self.state = State.STARTING
        self.thread = threading.Thread(target=self.run)
        self.thread.start()
        return self.start_future

    # awaitable
    def stop(self):
        assert self.state == State.RUNNING
        self.stop_future = asyncio.Future()
        self.state = State.STOPPING
        self.stop_event.set()
        return self.stop_future

    def _did_start(self):
        g_loop.call_soon_threadsafe(self.start_future.set_result, True)
        self.state = State.RUNNING

    def _did_fail_start(self, e):
        g_loop.call_soon_threadsafe(self.start_future.set_exception, e)

    def _did_stop(self):
        g_loop.call_soon_threadsafe(self.stop_future.set_result, True)
        self.is_running = False
        self.state = State.STOPPED

    def run(self):
        """This blocks until done"""
        try:
            self.browser_instance = BrowserInstance()
            self.browser_instance.start_browser(self.browser_name)
            self.browser_instance.join_call(self.room_url, self.user_name)
        except Exception as e:
            self._did_fail_start(e)
            return

        self._did_start()

        while self.state == State.RUNNING:
            self.stop_event.wait()

        self.browser_instance.quit_browser()
        self._did_stop()


class Node:
    def __init__(self):
        self.aiosession = aiohttp.ClientSession()
        self.browser = "firefox"
        self.node_name = None

        self.threads = set()
        self.thread_num = 0 # used for the username
        self.last_log_msg = None

        self.desired_thread_count = 0
        self.room_url = "https://meet.kde.org/b/nic-cnd-jtn"

        self.session_url = None

    def threads_in_state(self, *wanted_states):
        return len([t for t in self.threads if t.state in wanted_states])

    def threadcount_log_msg(self):
        return "starting:%d running:%d stopping:%d desired:%d" % (
                self.threads_in_state(State.STARTING),
                self.threads_in_state(State.RUNNING),
                self.threads_in_state(State.STOPPING),
                self.desired_thread_count
        )

    def threadcount_log(self):
        l.debug(self.threadcount_log_msg())

    def threadcount_log_if_changed(self):
        log_msg = self.threadcount_log_msg()
        if log_msg != self.last_log_msg:
            l.debug(log_msg)
            self.last_log_msg = log_msg

    async def start_stop_threads(self):
        """
        Starts or stops threads to make the number of running threads
        match the desired number
        """

        while self.threads_in_state(State.STARTING, State.RUNNING) < self.desired_thread_count:
            # start one!
            l.info("starting another")
            self.thread_num += 1
            t = BrowserThread()
            t.room_url = self.room_url
            if self.node_name:
                t.user_name = f"{self.node_name}-{self.thread_num}"
            else:
                t.user_name = f"test-{self.thread_num}"

            t.browser_name = self.browser
            asyncio.ensure_future(t.start()) # start it in the background (no await)
            self.threads.add(t)

        while self.threads_in_state(State.RUNNING) > self.desired_thread_count:
            # stop one!
            l.info("stopping one")

            t=next(t for t in self.threads if t.state == State.RUNNING)
            stop_f = t.stop()
            def make_stop_callback(t):
                def stop_callback(f):
                    l.info("stopped")
                    self.threads.remove(t)
                return stop_callback
            stop_f.add_done_callback(make_stop_callback(t))
            asyncio.ensure_future(stop_f)

    async def create_session(self):
        # reset the session URL so that calling this function always creates a new one
        self.session_url = None
        backoff=5

        while not self.session_url:
            try:
                req_data = {}
                if self.node_name:
                    req_data['name'] = self.node_name

                async with self.aiosession.post(BASEURL / "session", json=req_data) as response:
                    j = await response.json()
                    self.session_url = j['session_url']
            except Exception:
                l.warning("failed to connect to server; will retry in %d seconds", backoff)
                await asyncio.sleep(backoff)
                backoff *= random.gauss(2, 0.2)
                if backoff > 120: backoff /= 2

        l.info("created session %s", self.session_url)
        l.info("waiting for instructions from server...")

    async def main_loop(self):

        await self.create_session()

        while True:
            try:
                req_data = {
                    'starting': self.threads_in_state(State.STARTING),
                    'running': self.threads_in_state(State.RUNNING),
                    'stopping': self.threads_in_state(State.STOPPING)
                }

                async with self.aiosession.post(BASEURL.join(URL(self.session_url)), json=req_data) as response:
                    if response.status == 404:
                        l.info("Session not found? controller restarted?? getting new one...")
                        self.session_url = None
                        await self.create_session()
                        continue

                    resp_data = await response.json()

                self.desired_thread_count = resp_data["desired"]
                if "room_url" in resp_data:
                    self.room_url = resp_data["room_url"]

                self.threadcount_log_if_changed()
                await self.start_stop_threads()
                await asyncio.sleep(resp_data.get("backoff", 5))
            except aiohttp.client_exceptions.ClientError:
                print("failed to poll server, will retry...")
                await asyncio.sleep(5)

    async def stop(self):
        stop_coro = [t.stop() for t in self.threads if t.state==State.RUNNING]
        if len(stop_coro) > 0:
            await asyncio.wait(stop_coro)

        for t in self.threads:
            t.thread.join()

def get_available_browsers():
    supported_browsers = ['firefox', 'chrome']
    available_browsers = []
    l.info("Checking available browsers")
    for b in supported_browsers:
        inst = BrowserInstance()
        try:
            inst.start_browser(b)
            available_browsers.append(b)
            inst.quit_browser()
        except Exception:
            continue

    l.info("Found browsers: %r", available_browsers)

    return available_browsers

node=None
async def main():
    global node
    available_browsers = get_available_browsers()
    if len(available_browsers) == 0:
        l.error("No usable browsers found; maybe you're missing their webdriver modules?")
        return

    node = Node()
    node.browser = available_browsers[0]
    if len(sys.argv) > 1:
        node.node_name = sys.argv[1]

    await node.main_loop()

if __name__ == '__main__':
    g_loop = asyncio.get_event_loop()
    try:
        g_loop.run_until_complete(main())
    finally:
        l.warning("aborting!")
        g_loop.run_until_complete(node.stop())
