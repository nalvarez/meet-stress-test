# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import time
import uuid

logging.basicConfig(level=logging.DEBUG)
l = logging.getLogger('controller')

import json
from aiohttp import web

class NodeInfo:
    def __init__(self, node_id):
        self.node_id = node_id
        self.name = ""
        self.last_contact = 0
        self.instances_starting = 0
        self.instances_running = 0
        self.instances_stopping = 0
        self.instances_desired = 0
        self.ip_address = None

nodes = {}
backoff = 30
idle_timeout = 40

def create_node():
    new_id = str(uuid.uuid4())
    node_info = NodeInfo(new_id)
    nodes[node_info.node_id] = node_info
    return node_info

routes = web.RouteTableDef()

def do_auth(request):
    allowed_auth = ["Basic " + line.strip() for line in open("auth","r").readlines()]
    if request.headers.get('Authorization') not in allowed_auth:
        raise web.HTTPUnauthorized(headers={"WWW-Authenticate": "Basic"})

@routes.get('/controller.html')
async def controller_page(request):
    do_auth(request)
    return web.FileResponse("controller.html")

# called by node
@routes.post('/session/{id}')
async def session_fetch(request):
    node_id = request.match_info['id']
    if node_id not in nodes:
        result = {"error": "unknown session ID "+node_id}
        return web.json_response(result, status=404)

    data = await request.json()

    node_info = nodes[node_id]

    if 'starting' in data: node_info.instances_starting = int(data['starting'])
    if 'running'  in data: node_info.instances_running  = int(data['running'])
    if 'stopping' in data: node_info.instances_stopping = int(data['stopping'])

    node_info.last_contact = time.time()

    result = {
        "desired": node_info.instances_desired,
        "room_url": "https://meet-loadtest.kde.org/b/nic-cnd-jtn",
        "backoff": backoff
    }
    return web.json_response(result)

# called by node
@routes.post('/session')
async def create_session(request):
    global last_node_id
    data = await request.json()

    name = data.get("name", "unnamed")
    l.info("Creating session with name %r", name)

    node_info = create_node()
    node_info.name = name
    node_info.last_contact = time.time()

    result = {'node_id': node_info.node_id, 'session_url': f'/session/{node_info.node_id}'}
    return web.json_response(result)

# called by webpage
@routes.get('/info')
async def get_info(request):
    do_auth(request)

    result = {'nodes': []}
    for node in nodes.values():
        # if we haven't heard of a node in a while, don't return it
        if node.last_contact < time.time() - idle_timeout:
            continue

        result['nodes'].append({
            'id': node.node_id,
            'name': node.name,
            'starting': node.instances_starting,
            'running': node.instances_running,
            'stopping': node.instances_stopping,
            'desired': node.instances_desired
        })

    return web.json_response(result)

# called by webpage
@routes.post('/set')
async def set_api(request):
    do_auth(request)

    data = await request.json()
    if 'id' in data and 'desired' in data:
        if data['id'] not in nodes:
            result = {"error": "unknown session ID "+data['id']}
            return web.json_response(result, status=404)

        nodes[data['id']].instances_desired = int(data['desired'])

    if 'backoff' in data:
        global backoff
        backoff = int(data['backoff'])
        l.info("Changed backoff interval to %r", backoff)

    if 'idle_timeout' in data:
        global idle_timeout
        idle_timeout = int(data['idle_timeout'])
        l.info("Changed idle timeout to %r", idle_timeout)

    result = {'status': 'ok'}
    return web.json_response(result)

app = web.Application()
app.add_routes(routes)
web.run_app(app, port=12345)
